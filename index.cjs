const checkbox = document.querySelector(".checkbox");
const priceBasic = document.querySelector("#basicAmount");
const pricePro = document.querySelector("#professionalAmount");
const priceMaster = document.querySelector("#masterAmount");

checkbox.addEventListener("click", () => {
  if (checkbox.checked) {
    priceBasic.innerText = "19.99";
    pricePro.innerText = "24.99";
    priceMaster.innerText = "39.99";
  } else {
    priceBasic.innerText = "199.99";
    pricePro.innerText = "249.99";
    priceMaster.innerText = "399.99";
  }
});
